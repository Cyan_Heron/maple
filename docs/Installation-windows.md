
1.0 https://holistic3d.com/news-tips/tensorflow/ 
	Postępuj zgodnie z instrukcją od (Ctrl+F) { Windows 1. Download }
	Aż do miejsca (Ctrl+F) { 13.  Still with me?  Patience is a virtue. 🙂 }

1.1 Wersje urządzeń

		Nvidia CUDA 9.0
		Nvidia cudnn 7.1.4

2.! Instalację Anaconda MUSICIE ZROBIĆ PRZEZ URUCHOM JAKO ADMINISTRATOR !!!!!
2.0  https://holistic3d.com/news-tips/tensorflow/ 
	Postępuj zgodnie z instrukcją od (Ctrl+F) { 14.  Now its time to install the python environment, Anaconda. }
	Aż do miejsca (Ctrl+F) { 17.  Close the command prompt window and open it again. }
	
2.1 W zmiennej systemowej "Path" powinny być:

	C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\lib\x64
	C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\extras\CUPTI\libx64
	C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0
	C:\Program Data\Anaconda3\Scripts
	C:\Program Data\Anaconda3\Scripts\conda.exe
	C:\Program Data\Anaconda3
	C:\Program Data\Anaconda3\python.exe

3.0 Uruchom konsolę 'Anaconda Prompt' jako ADMINISTRATOR
3.1 Wpisz: conda create -n ml python=3.6
3.2 Wpisz: activate ml

4.0 Pobierz repozytorium https://github.com/Unity-Technologies/ml-agents/archive/0.6.0a.zip
4.1 W 'Anaconda Prompt' wpisz: cd C:\Downloads\ml-agents-0.6.0a\ml-agents
4.2 Wpisz: pip install . (tu jest ta kropka)
4.3 Wpisz także: pip install pypiwin32
4.4* Dla testu sprawdź w 'Anaconda Prompt' czy komenda: 'mlagents-learn --help' nie robi błędów

5.1 W 'Anaconda Prompt' wpisz : pip install tensorflow-gpu==1.5.0
5.2 Przetestuj wpisując kolejno:
		python
		import tensorflow as tf
		sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
	Nie powinno dać to błedów i powinno wyświetlić literki GPU albo nazwę karty graficznej gdzieś w ściane tekstu
5.3 Zakończ wpisując: exit()
	
6.0 Pobrać Unity w wersji 2017.4.10f1	
6.1 Projekt Unity przez otworzyć można przez "Open"
	zaznaczyć folder C:\Downloads\ml-agents-0.6.0a\UnitySDK
	przycisnąć 'Open'
6.2 Wchodzimy na stronę https://s3.amazonaws.com/unity-ml-agents/0.3/TFSharpPlugin.unitypackage
	pobieramy co tam jest
	uruchamiamy przy włączonym Unity
6.3 Otworzyć scenę Assets/ML-Agents/Examples/3DBall/Scenes/3DBall
6.4 W Assets/ML-Agents/Examples/3DBall/Prefabs wybrać Platform i w inspektorze w polu 'Ball 3D Agent' zmienić Brain na 3DBallLearning
6.5 W Hierarchy wybrać Ball3DAcademy i w inspektorze wybrać w Brains 3DBallLearning i zaznaczyć kwadracik Control
6.6 Wejść w File/BuildSettings 
		kliknij Add Open Scenes  (powinna być tylko 1 zaznaczona na liście w polu powyżej)
		zaznacz okienko Development Build i kliknij Build
6.7 Wybierz ścieżkę na C:/Downloads/ml-agents-0.6.0a i nazwij go ball.exe

7.0 Wejdź w 'Anaconde Prompt' i przedź do folderu C:\Downloads\ml-agents-0.6.0a
7.1 Wpisz jak chcesz uruchomić uczenie: mlagents-learn .\config\trainer_config.yaml --env=ball --train 
7.2 Wpisz jak chcesz uruchomić agenta nauczonego: mlagents-learn .\config\trainer_config.yaml --env=ball --load --slow

8.1 Jak chcesz zobaczyć benchmarki uczenia to w osobnym 'Anaconda prompt' przejdź do folderu C:\Downloads\ml-agents-0.6.0a 
	Wpisz: tensorboard --logdir=summaries
8.2 Odpal przeglądarkę tam gdzie tobie podpowie (np. http://desktop-04qldsq:6006/ )


9.**. Tu jest info do zarządzania ale na trochę inna wersję:
  https://www.youtube.com/playlist?list=PLX2vGYjWbI0R08eWQkO7nQkGiicHAX7IX
