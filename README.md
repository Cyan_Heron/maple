# Introduction

MAPLE stands for **Multi-Agent Pursuit in the Labirynth Environment**.

This repository consists of the works of: 
* Paweł Belczak
* Mikołaj Kmieć
* Maciej Żurek

# Instalation
For troubleshooting refer to this guide [1].

* Download Unity 2018.4.4f [2]
* Download ml-agents 0.8.2 [3]
* Download Anaconda [4]
---
* Create Anaconda environment:
```
conda create -n maple python=3.6
```
* Activate the environment
```
activate maple
```
* Install Tensorflow
```
pip install tensorflow==1.7.1
```
or Tensorflow GPU
```
pip install tensorflow-gpu==1.7.1
```
* Install ml-agents
```
pip install mlagents
```
---
* Clone this repository
* Unzip ml-agents to a handy location
* Copy `ML-Agents` folder from `ml-agents-0.8.2/UnitySDK/Assets` to `maple/Maple/Assets`
* Open `maple/Maple` folder in Unity
---
[1] https://github.com/Unity-Technologies/ml-agents/blob/master/docs/Installation-Windows.md (Windows)
[2] https://unity3d.com/get-unity/download?thank-you=update&download_nid=62716&os=Win (Windows)
[3] https://github.com/Unity-Technologies/ml-agents/archive/0.8.2.zip
[4] https://repo.anaconda.com/archive/Anaconda3-2019.07-Windows-x86_64.exe