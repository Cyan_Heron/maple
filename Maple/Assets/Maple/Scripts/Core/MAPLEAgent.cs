﻿using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public abstract class MAPLEAgent : Agent
{
    [Header("Specific to MAPLE")]
	public int raysNumber;
	public int rayWallLayer;
	public int rayPursuerLayer;
    public int rayPursueeLayer;
    public int rayDimension;
   
    public int rayRange;

	public bool seePursuersThroughWalls;
    public bool seePursueesThroughWalls;

	[HideInInspector]
	public Vector3 initialLocation;

	protected DynamicMovement dm;
	protected StageController stageController;

	public List<RayLineController> rlcs;
	public List<RayValueController> rvcs;

// Abstract

	public abstract void InitialiseLayers();
	public abstract void InitialiseObjects();
	public abstract void GatherInput();
	public abstract void Reward();

	public override void AgentOnDone()
	{
		base.AgentOnDone();
		Debug.Log("Done");
	}


	public void Awake()
	{
		initialLocation = gameObject.transform.position;
		stageController = transform.parent.GetComponentInParent<StageController>();
	}
	
	public override void InitializeAgent()
	{
		base.InitializeAgent();

		InitialiseLayers();

		dm = gameObject.GetComponent<DynamicMovement>();

		InitialiseObjects();

		GatherInput();
	}

	/*Wektor obserwacji - stała wielkość*/
    public override void CollectObservations()
    {
		for (int i = 0; i < rvcs.Count; i++)
		{
			for (int j = 0; j < rvcs[i].values.Length; j++)
			{
				AddVectorObs(rvcs[i].values[j]);
			}
		}
	}

	/*Akcja i nagroda*/
	public override void AgentAction(float[] vectorAction, string textAction)
    {    
        if (brain.brainParameters.vectorActionSpaceType == SpaceType.continuous)
        {
			// Action Rotation
			float val = Mathf.Clamp(vectorAction[0], -1, 1);
			float angle = val/2; // (float)MathUtils.SignThreshold(val, 0.01);
			
			if (brain is PlayerBrain){
				if (Input.GetKey(KeyCode.LeftShift)){
					angle *= 1.0f;
				}
				else{
					angle *= 0.5f;
				}
			}
			dm.rotate(angle);

			// Action Change in velocity
			val = Mathf.Clamp(vectorAction[1], 0, 1);
			if (brain is PlayerBrain){
				if (!Input.GetKey(KeyCode.LeftControl)){
					val *= 0.75f;
				}
			}
			dm.accelerate(val);
        }

		Reward();
		GatherInput();
	}

    public override void AgentReset() // happens after new Env and Relocation has been generated
    {
		dm.reset();
		stageController.Restart();
	}


	public DynamicMovement GetDynamicMovement()
	{
		return dm;
	}

	public List<float> GetVectorObs()
	{
		List<float> observations = new List<float>();
		for (int i = 0; i < rvcs.Count; i++)
		{
			for (int j = 0; j < rvcs[i].values.Length; j++)
			{
				observations.Add(rvcs[i].values[j]);
			}
		}
		return observations;
	}
}
