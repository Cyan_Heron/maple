﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System;

public class MAPLEPursuer : MAPLEAgent
{
    [Header("Specific to MAPLE")]

    private MAPLEPursuee pursuee;
	private static Func<float, float> adapted = x => Mathf.Min(1f / Mathf.Sqrt(10*x), 1f);
	private Func<float, float> distanceFunction = adapted;
	private const bool supported = true;

	public override void InitialiseLayers()
	{
		// Appropriate representaion of layers
		rvcs = new List<RayValueController>();
		rvcs.AddS(new RayValueController(raysNumber, distanceFunction))
			.AddS(new RayValueController(raysNumber, distanceFunction))
			.AddS(new RayValueController(raysNumber, distanceFunction))
			.AddS(new RayValueController(1, distanceFunction));

		// Detailed representation of layers
		rlcs = new List<RayLineController>();
		rlcs.AddS(new RayLineController(RayLineController.LayerIndex.WALL, raysNumber))
			.AddS(new RayLineController(RayLineController.LayerIndex.PURSUER, raysNumber))
			.AddS(new RayLineController(RayLineController.LayerIndex.PURSUEE, raysNumber))
			.AddS(new RayLineController(RayLineController.LayerIndex.OTHER, 1));

		// Visual representation of layers
		RayLineDrawerUI rldui = gameObject.GetComponent<RayLineDrawerUI>();
		rldui.SetList(new List<RayLineDrawer>()
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.WALL,
					rlcs[(int)RayLineController.LayerIndex.WALL],
					new Vector3(0f, 0.5f, 0f))
				.SetGradient(0.4f, 0f, distanceFunction)
				.SetHideMisses(true)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.PURSUER,
					rlcs[(int)RayLineController.LayerIndex.PURSUER],
					new Vector3(0f, 0.525f, 0f))
				.SetGradient(0.75f, 0.5f, distanceFunction)
				.SetHideMisses(true)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.PURSUEE,
					rlcs[(int)RayLineController.LayerIndex.PURSUEE],
					new Vector3(0f, 0.550f, 0f))
				.SetGradient(1f, 0.75f, distanceFunction)
				.SetHideMisses(true)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.OTHER,
					rlcs[(int)RayLineController.LayerIndex.OTHER],
					new Vector3(0f, 0.575f, 0f))
				.SetColor(Color.black)
				.SetScaling(3f, 1f)
			)
		);
	}

	public override void InitialiseObjects()
	{
		pursuee = GameObject.Find("Pursuees").GetComponentsInChildren<MAPLEPursuee>()[0];
	}

	public override void GatherInput()
	{
		GatherInputCast(
			new List<IRayed>()
				.AddIf(rlcs[(int)RayLineController.LayerIndex.WALL], supported)
				.AddS(rvcs[(int)RayLineController.LayerIndex.WALL]),
			null,
			rayWallLayer);

		GatherInputCast(
			new List<IRayed>()
				.AddIf(rlcs[(int)RayLineController.LayerIndex.PURSUER], supported)
				.AddS(rvcs[(int)RayLineController.LayerIndex.PURSUER]),
			null,
			rayPursuerLayer);

		GatherInputCast(
			new List<IRayed>()
				.AddIf(rlcs[(int)RayLineController.LayerIndex.PURSUEE], supported)
				.AddS(rvcs[(int)RayLineController.LayerIndex.PURSUEE]),
			null,
			rayPursueeLayer);

		if (supported)
		{
			rlcs[(int)RayLineController.LayerIndex.OTHER].Ray(0, dm.getVelocity(), 1f, false);
			rlcs[(int)RayLineController.LayerIndex.OTHER].RayParams(0,
				gameObject.transform.position,
				gameObject.transform.TransformDirection(Vector3.forward));
		}
		rvcs[(int)(int)RayLineController.LayerIndex.OTHER].values[0] = dm.getVelocity();
	}

	public override void Reward() // Repeats 5 times ignoring AgentResets
	{
		if (Vector3.Distance(gameObject.transform.position, pursuee.gameObject.transform.position)
		   < 1f
		    ){
			//Debug.Log("WIN");
			stageController.Win();
		}
	}

	private void GatherInputCast(List<IRayed> irs, float[] limit, int layer)
	{
		RayCaster.CastAround(
			irs,
			raysNumber,
			gameObject.transform.position,
			gameObject.transform.TransformDirection(Vector3.forward),
			rayRange,
			limit,
			layer);
	}
}
