﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System;

public class MAPLEAcademy : Academy
{
	private System.Random random;
	private StageController[] stages;

	public override void InitializeAcademy()
	{
		base.InitializeAcademy();
		random = new System.Random();
	}

	public void SetAcademyState(SceneController sc)
	{
		stages = sc.stages;
		foreach (StageController stage in stages)
		{
			ParametersUpdate(stage);
		}
	}

	public override void AcademyReset()
    {
		foreach (StageController stage in stages) {
			stage.Finish(true);
			ParametersUpdate(stage);
		}
    }

	public override void AcademyStep()
    {
		foreach (StageController stage in stages)
		{
			stage.Step();
		}
	}


	public void ParametersUpdate(StageController stage)
	{
		foreach (MAPLEPursuee p in stage.GetPursuees())
		{
			p.GetDynamicMovement().agentMovementScale = resetParameters["velocityOfPrey"];
		}
		foreach (MAPLEPursuer p in stage.GetPursuers())
		{
			
		}
		IGenerated[] ig = stage.GetEnvironmentController().GetGenerators();

		foreach (IGenerated i in ig)
		{
			if (i is ObstacleGenerator)
			{
				(i as ObstacleGenerator).numberOfObstacles = (int)resetParameters["obstacleCount"];
			}
		}
	}
}
