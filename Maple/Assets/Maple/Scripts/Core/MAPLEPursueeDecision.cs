﻿using MLAgents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MAPLEPursueeDecision : Decision {

	public bool dummy;
	public MAPLEPursuee reference;

	private int numOfPursuers;
	public Transform[] pursuers;
	public int raysNumber;

	public bool[] sectors;

	public float[] action;
	public enum ActionIndex
	{
		ROTATION = 0,
		ACCEL,
		LENGTH
	}

	private class Gap
	{
		public int beggining;
		public int end;
		public int middle;
		public int length;
	}
	public void Awake()
	{
		dummy = false;
		action = new float[2];
		// Get all pursuers
		reference = GameObject.Find("Pursuee").GetComponent<MAPLEPursuee>();
		GameObject Pursuers = GameObject.Find("Pursuers");
		numOfPursuers = Pursuers.transform.childCount;
		pursuers = new Transform[numOfPursuers];
		for (int i = 0; i < numOfPursuers; i++)
		{
			pursuers[i] = Pursuers.transform.GetChild(i);
			//Debug.Log("Puruser " + i + ": " + pursuers[i].name);
		}
		reference.absoluteEscapeDirection = new Vector3();
		raysNumber = reference.raysNumber;
		// VFH
		sectors = new bool[raysNumber];
	}

	public override float[] Decide(List<float> vectorObs, List<Texture2D> visualObs, float reward, bool done, List<float> memory)
	{
		// Initial reset
		action[0] = 0;
		action[1] = 0;

		Vector3 escapeDirection = new Vector3(0, 0, 0);
		int raysNumber = reference.raysNumber;

		// Count distance to all pursuers
		foreach (Transform pursuer in pursuers)
		{
			Vector3 heading = reference.gameObject.transform.position - pursuer.gameObject.transform.position;
			if (heading.magnitude < reference.lionSenseDistance)
			{
				escapeDirection += heading;
			}
		}

		if (!escapeDirection.Equals(new Vector3(0, 0, 0)))
		{
			if (reference.useOldVFF)
			{
				for (int j = 0; j < raysNumber; j++)
				{
					if (vectorObs[j] != 0f)
					{
						//	Debug.Log (j + " " + rld.Get (i, j));
						RayLine tmp = reference.rlcs[0].rayLines[j];
						escapeDirection += -tmp.direction / (tmp.length * tmp.length);
					}
				}
			}
			else
			{ // VFH

				// find the nearest sector for the escapeDirection
				float angle = Vector3.SignedAngle(Vector3.forward, escapeDirection, Vector3.up);
				if (angle < 0)
					angle += 360f;
				float sectorWidth = 360f / raysNumber;
				int nearestSector = (int)(angle / sectorWidth);
				//				Debug.Log ("Nearest sector: " + nearestSector);

				// check if sectors are free or blocked
				for (int j = 0; j < vectorObs.Count; j++)
				{
					if (vectorObs[j] != 0f)
					{
						sectors[j] = true;
					}
					else
					{
						sectors[j] = false;
					}
				}
				Debug.Log(sectors);

				// find all the gaps
				bool inThegap = false;
				List<Gap> gaps = new List<Gap>();
				Gap currentGap = new Gap();
				for (int j = 0; j < raysNumber; j++)
				{
					if (sectors[j] == false)
					{
						if (inThegap)
						{
							currentGap.end = j;
							currentGap.length += 1;
						}
						else
						{
							inThegap = true;
							currentGap.beggining = j;
							currentGap.end = j;
							currentGap.length = 1;
						}
					}
					else
					{
						if (inThegap)
						{
							currentGap.middle = currentGap.beggining + (currentGap.length / 2);
							gaps.Add(currentGap);
							currentGap = new Gap();
							inThegap = false;
						}
					}
				}

				// check the last gap;
				if (inThegap)
				{
					if (gaps.Count != 0 && gaps[0].beggining == 0)
					{
						gaps[0].beggining = currentGap.beggining;
						gaps[0].length += currentGap.length;
						gaps[0].middle = gaps[0].beggining + (gaps[0].length / 2);
						if (gaps[0].middle >= raysNumber)
							gaps[0].middle -= raysNumber;
					}
					else
					{
						currentGap.middle = currentGap.beggining + (currentGap.length / 2);
						gaps.Add(currentGap);
					}
				}

				// find nearest gap
				int abs = 0;
				int gapIndex = 0;
				int minOffset = raysNumber;
				for (int k = 0; k < gaps.Count; k++)
				{
					abs = nearestSector - gaps[k].middle;
					if (abs < 0)
						abs *= -1;
					if (abs < minOffset)
					{
						minOffset = nearestSector - gaps[k].middle;
						gapIndex = k;
					}
				}

				escapeDirection = Quaternion.AngleAxis(gaps[gapIndex].middle * sectorWidth, Vector3.up) * Vector3.forward;
			}


			//// After All

			escapeDirection.Normalize();
			reference.absoluteEscapeDirection = escapeDirection;
			if(!dummy)reference.GetDynamicMovement().rotateTowards(escapeDirection);

			if (Vector3.Angle(reference.GetDynamicMovement().getDirection(), escapeDirection) < 90)
			{
				action[(int)ActionIndex.ACCEL] = 1f;
			}
		}
		else
		{
			/*Empty*/
		}

		return action;
	}

	public override List<float> MakeMemory(List<float> vectorObs, List<Texture2D> visualObs, float reward, bool done, List<float> memory)
	{
		return new List<float>();
	}
	
}
