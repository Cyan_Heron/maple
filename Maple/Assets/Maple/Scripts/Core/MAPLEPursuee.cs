﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using System;

public class MAPLEPursuee : MAPLEAgent
{
	private MAPLEPursueeDecision decisionViewer;

	public float lionSenseDistance;
	[HideInInspector]
	public Vector3 absoluteEscapeDirection;
	public bool useOldVFF;

	private List<RayLineController> nonCasted;
	
	private static Func<float, float> adapted = x => Mathf.Min(1f / (10 * x), 1f);
	private Func<float, float> distanceFunction = adapted;
	private const bool supported = true;

	public override void InitialiseLayers()
	{
		// Appropriate representaion of layers
		rvcs = new List<RayValueController>();
		rvcs.AddS(new RayValueController(raysNumber, distanceFunction))
			.AddS(new RayValueController(raysNumber, distanceFunction))
			.AddS(new RayValueController(1, distanceFunction));

		// Detailed representation of layers
		rlcs = new List<RayLineController>();
		rlcs.AddS(new RayLineController(RayLineController.LayerIndex.WALL,raysNumber))
			.AddS(new RayLineController(RayLineController.LayerIndex.PURSUER, raysNumber))
			.AddS(new RayLineController(RayLineController.LayerIndex.OTHER, 1));

		nonCasted = new List<RayLineController>();
		nonCasted.AddS(new RayLineController(RayLineController.LayerIndex.OTHER, 1));

		// Visual representation of layers
		RayLineDrawerUI rldui = gameObject.GetComponent<RayLineDrawerUI>();
		rldui.SetList(new List<RayLineDrawer>()
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.WALL,
					rlcs[(int)RayLineController.LayerIndex.WALL],
					new Vector3(0f, 0.5f, 0f))
				.SetGradient(0.4f, 0f, distanceFunction)
				.SetHideMisses(true)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.PURSUER,
					rlcs[(int)RayLineController.LayerIndex.PURSUER],
					new Vector3(0f, 0.525f, 0f))
				.SetGradient(0.75f, 0.5f, distanceFunction)
				.SetHideMisses(true)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.OTHER,
					rlcs[(int)RayLineController.LayerIndex.PURSUEE],
					new Vector3(0f, 0.550f, 0f))
				.SetColor(Color.blue)
				.SetScaling(3f, 1f)
			)
			.AddS(
				new RayLineDrawer(
					RayLineDrawer.LayerIndex.OTHER,
					nonCasted[0],
					new Vector3(0f, 0.575f, 0f))
				.SetColor(Color.black)
				.SetScaling(3f, 1f)
			)
		);
	}

	public override void InitialiseObjects()
	{
		decisionViewer = ScriptableObject.CreateInstance<MAPLEPursueeDecision>();
		decisionViewer.dummy = true;
	}

	public override void GatherInput()
	{
		GatherInputCast(
			new List<IRayed>()
				.AddIf(rlcs[(int)RayLineController.LayerIndex.WALL], supported)
				.AddS(rvcs[(int)RayLineController.LayerIndex.WALL]),
			null,
			rayWallLayer, rayRange);

		GatherInputCast(
			new List<IRayed>()
				.AddIf(rlcs[(int)RayLineController.LayerIndex.PURSUER], supported)
				.AddS(rvcs[(int)RayLineController.LayerIndex.PURSUER]),
			null,
			rayPursuerLayer, lionSenseDistance);

		if (supported)
		{
			rlcs[(int)RayLineController.LayerIndex.PURSUEE].Ray(0, 1f, 1f, false);
			rlcs[(int)RayLineController.LayerIndex.PURSUEE].RayParams(0,
				gameObject.transform.position,
				absoluteEscapeDirection);
			nonCasted[0].Ray(0, dm.getVelocity(), 1f, false);
			nonCasted[0].RayParams(0,
				gameObject.transform.position,
				dm.getDirection());
		}
	}

	public override void Reward()
	{
		if (brain is PlayerBrain)
		{
			decisionViewer.Decide(GetVectorObs(), null, 0f, false, null);
			Debug.Log("dumming");
		}
	}


	private void GatherInputCast(List<IRayed> irs, float[] limit, int layer, float rayRange)
	{
		RayCaster.CastAround(
			irs,
			raysNumber,
			gameObject.transform.position,
			gameObject.transform.TransformDirection(Vector3.forward),
			rayRange,
			limit,
			layer);
	}

	public override void AgentReset()
	{
		base.AgentReset();
		transform.position = this.initialLocation;
	}
}
