﻿using UnityEngine;

public interface IRayed
{
	void Ray(int index, float distance, float coverage, bool hit);
	void RayParams(int index, Vector3 position, Vector3 direction);
}