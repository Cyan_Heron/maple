﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowingController : MonoBehaviour {

	[ReadOnly]
	public MAPLEAgent agent;
	public SceneController bc;

	private Vector3 positionTemp;
	private Vector3 rotationTemp;

	private Vector3 positionInit;
	private Vector3 rotationInit;

	public bool FPP;
	public bool cameraRotation;
	public bool TPP;

	public float sensitivityX;
	public float sensitivityY;

	private float zoom;

	private float vlookAngle;
	private float hlookAngle;

	private Vector3 lastPosition;

	// Use this for initialization
	void Awake () {
		positionTemp = gameObject.transform.position;
		rotationTemp = gameObject.transform.rotation.eulerAngles;

		positionInit = positionTemp;
		rotationInit = rotationTemp;

		vlookAngle = 0f;
		hlookAngle = 0f;
		zoom = 0f;
	}

	/*public float CalculateFov() { /// Na kamerze 2D ?????
		// (float width, float length) //DONE 
		
		float width = 60;
		float length = 45;

		float q, finalFOV;

		if (width / length > c.aspect)
			q = width / c.aspect;
		else
			q = length;

		float distance = gameObject.transform.position.y;

		float arg = (q * 0.5f / distance) * 1.1f; //to make board more visable
		finalFOV = 2.0f * Mathf.Atan(arg) * Mathf.Rad2Deg;

		return finalFOV;
	}*/

	// Update is called once per frame
	void Update()
	{
		agent = bc.AccessAgent();
		if (agent != null)
		{
			if (FPP || TPP)
			{
				gameObject.transform.position = agent.gameObject.transform.position + new Vector3(0, 1.75f, 0);
				gameObject.transform.rotation = Quaternion.Euler(
					new Vector3(vlookAngle, 0, 0) 
					+ new Vector3(0, hlookAngle, 0)
					+ agent.gameObject.transform.localRotation.eulerAngles
				);

				// Over shoulder view
				if (TPP)
				{
					float TPPdistance = 3f - zoom;
					float alpha = Mathf.Deg2Rad * gameObject.transform.rotation.eulerAngles.y;
					gameObject.transform.position += Vector3.back * TPPdistance * Mathf.Cos(alpha)
											  + Vector3.left * TPPdistance * Mathf.Sin(alpha);
				}
			}
			else
			{
				positionTemp = agent.gameObject.transform.position;
				positionTemp.y = gameObject.transform.position.y;
				gameObject.transform.position = positionTemp;

				if (cameraRotation)
				{
					rotationTemp.y = agent.gameObject.transform.rotation.eulerAngles.y;
					gameObject.transform.rotation = Quaternion.Euler(rotationTemp);
				}
				else
				{
					rotationTemp.y = 0f;
					gameObject.transform.rotation = Quaternion.Euler(rotationTemp);
				}
			}

		}
		else
		{
			gameObject.transform.position = positionInit;
			gameObject.transform.rotation = Quaternion.Euler(rotationInit);
		}

		if (Input.GetKey(KeyCode.LeftControl) && Input.mousePosition.y != lastPosition.y)
		{
			vlookAngle += (lastPosition.y - Input.mousePosition.y) * sensitivityY;
			vlookAngle = Mathf.Clamp(vlookAngle, -25, 75);
		}

		lastPosition = Input.mousePosition;
	}

	private void OnPreRender()
	{
		if (FPP && agent != null)
			agent.transform.GetChild(0).gameObject.SetActive(false);
	}
	private void OnPostRender()
	{
		if (FPP && agent != null)
			agent.transform.GetChild(0).gameObject.SetActive(true);
	}

}
