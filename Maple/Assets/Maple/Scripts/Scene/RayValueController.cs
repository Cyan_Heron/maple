﻿using System;
using UnityEngine;

public class RayValueController : IRayed
{
	public float[] values;
	public float[] distance;

	private Func<float, float> valueFunc;

	public RayValueController(int size, Func<float, float> valueFunc = null)
	{
		this.values = new float[size];
		this.distance = new float[size];

		this.valueFunc = valueFunc ?? Macro.IDENTITY;
	}

	// IRayed
	public void Ray(int index, float distance, float coverage, bool hit)
	{
		this.values[index] = (!hit)?
			0f : valueFunc(coverage);
		this.distance[index] = distance;
	}

	public void RayParams(int index, Vector3 position, Vector3 direction)
	{
		//Ignore
	}
}
