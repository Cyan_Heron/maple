﻿using System;
using UnityEngine;

public class RayLineDrawer
{
	private readonly LayerIndex layerIndex;
	private readonly RayLineController rlc;

	public bool visible = true;

	private Vector3 offset;
	private float scale = 1f, extrusion = 0f;
	private bool hideMisses = false;
	private Func<float, float> colorFunc = Macro.IDENTITY;

	private float fromColor = 0f, toColor = 0f;
	private Color color = Color.black;
	bool gradientOrColor; // false - color, true - grad // TODO SEPARATE GRADING CLASS

	public RayLineDrawer(LayerIndex layerIndex, RayLineController rlc, Vector3 offset)
	{
		this.layerIndex = layerIndex;
		this.rlc = rlc;
		this.offset = offset;

		CameraLineDrawer cld = GameObject.Find("Main Camera").GetComponent<CameraLineDrawer>();
		cld.Register(this, this.GetHashCode());
	}

	public LayerIndex GetLayerIndex()
	{
		return layerIndex;
	}

	// Builder
	public RayLineDrawer SetGradient(float fromColor, float toColor, Func<float, float> colorFunc = null)
	{
		this.fromColor = fromColor;
		this.toColor = toColor;
		gradientOrColor = true;

		this.colorFunc = colorFunc ?? Macro.IDENTITY;
		return this;
	}

	public RayLineDrawer SetColor(Color color)
	{
		this.color = color;
		gradientOrColor = false;
		return this;
	}

	public RayLineDrawer SetScaling(float scale, float extrusion = 0f)
	{
		this.scale = scale;
		this.extrusion = extrusion;
		return this;
	}

	public RayLineDrawer SetHideMisses(bool b)
	{
		this.hideMisses = b;
		return this;
	}

	public RayLineDrawer SetVisibility(bool b)
	{
		visible = b;
		return this;
	}


	// Etc
	public void Draw()
	{
		DrawProcess((position, direction, offset, length, color) =>
		{
			GL.Color(color);
			GL.Vertex(position + offset);
			GL.Vertex(position + offset + direction * length);
		});
	}

	public void DrawDebug()
	{
		DrawProcess((position, direction, offset, length, color) =>
		{
			Debug.DrawRay(
				position + offset,
				direction * length,
				color);
		});
	}

	private void DrawProcess(Action<Vector3, Vector3, Vector3, float, Color> func)
	{
		if (!visible) return;
		for (int i = 0; i < rlc.Length(); i++)
		{
			RayLine target = rlc.GetRayLine(i);
			float length =
				(!target.hit && hideMisses) ?
				0f : target.length * scale + extrusion;

			if (length == 0f) continue;

			Color color;
			if (gradientOrColor)
			{ // gradient // TODO Separate Class
				float colorDelta = toColor - fromColor; // Color gradation
				color = Color.HSVToRGB(fromColor + colorDelta * colorFunc(target.coverage), 1f, 1f);
			}
			else
			{ // color
				color = this.color;
			}
			func(target.position, target.direction, offset, length, color);
		}
	}

	public enum LayerIndex
	{
		WALL = 0,
		PURSUER,
		PURSUEE,
		OTHER,
		LENGTH // Must end in LENGTH ( enum performance hack )
	}
}