﻿using System;
using UnityEngine;

public class RayLine {

	public Vector3 position;
	public Vector3 direction;

	public float coverage;
	public float length;

	public bool hit;

	public RayLine()
	{
		this.length = 0f;
	}
}
