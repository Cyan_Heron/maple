﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RayLineDrawerUI : MonoBehaviour
{
	public bool wallLayer;
	public bool pursuerLayer;
	public bool pursueeLayer;
	public bool movementLayer;

	private bool disabled;
	private List<RayLineDrawer> list = null;

	private void Awake()
	{
		disabled = !enabled;
	}

	public void OnEnable()
	{
		disabled = false;
		OnValidate();
	}

	public void OnDisable()
	{
		disabled = true;
		OnValidate();
	}

	public void OnValidate()
	{
		if (list == null) return;
		for (int i = 0; i < list.Count; i++)
		{
			list[i].SetVisibility(GetAssociatedValue(list[i]) && !disabled);
		}
	}

	public void SetList(List<RayLineDrawer> list)
	{
		this.list = list;
		OnValidate();
	}

	private bool GetAssociatedValue(RayLineDrawer item)
	{
		switch (item.GetLayerIndex())
		{
			case RayLineDrawer.LayerIndex.WALL: return wallLayer;
			case RayLineDrawer.LayerIndex.PURSUER: return pursueeLayer;
			case RayLineDrawer.LayerIndex.PURSUEE: return pursueeLayer;
			case RayLineDrawer.LayerIndex.OTHER: return movementLayer;
			default: throw new System.Exception("MAPLE Exception in RayLineDrawerUI. Layer not registered");
		}
	}
}