﻿using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class CameraLineDrawer : MonoBehaviour
{
	public Material lineMaterial;
	private Dictionary<int, RayLineDrawer> lines;

	public CameraLineDrawer(){
		lines = new Dictionary<int, RayLineDrawer> ();
    }

	public void Register(RayLineDrawer rlc, int id){
		if(!lines.ContainsKey(id))
			lines.Add (id, rlc);
	}
	public void Unregister(int id){
		if(lines.ContainsKey(id))
			lines.Remove (id);
	}

	void OnPostRender(){
		lineMaterial.SetPass (0);
		GL.PushMatrix ();
		GL.Clear (false, false, Color.white );
        GL.Begin (GL.LINES);
		foreach(KeyValuePair<int, RayLineDrawer> entry in lines){
			entry.Value.Draw();
		}
		GL.End ();
		GL.PopMatrix ();
	}

	private void Update()
	{
		foreach (KeyValuePair<int, RayLineDrawer> entry in lines){
			entry.Value.DrawDebug();
		}
	}
}
