﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Static class
public class RayCaster {

	public static void CastAround(
		List<IRayed> irs,
		int raysNumber,
		
		Vector3 position,
		Vector3 orientation,

		float generalLimit,
		float[] limit,
		int mask
	)
	{
		int layerMask = 1 << mask;
		for (int i = 0; i < raysNumber; i++)
		{
			float rayRange = generalLimit;
			if (limit != null && limit[i] < generalLimit) rayRange = limit[i];
			Vector3 direction = MathUtils.RotateVector3(orientation, i * 360f / raysNumber);

			Cast(irs, i,
				position, direction,
				generalLimit, rayRange,
				layerMask);
		}
	}

	private static void Cast(
		List<IRayed> irs,
		int index,

		Vector3 position,
		Vector3 orientation,

		float generalLimit,
		float limit,
		int mask
		)
	{
		RaycastHit rh;
		float rayRange = limit;

		bool hit = Physics.Raycast(position, orientation, out rh, rayRange, mask);
		foreach (IRayed ir in irs)
		{
			ir.Ray(index, rh.distance, rh.distance/generalLimit, hit);
			ir.RayParams(index, position, orientation);
		}
	}
}
