﻿using System;
using UnityEngine;

public class RayLineController : IRayed
{
	private LayerIndex layerIndex;
	public RayLine[] rayLines;

	public RayLineController(LayerIndex li, int size)
	{
		this.layerIndex = li;
		rayLines = new RayLine[size];
		for (int i = 0; i < size; i++)
		{
			rayLines[i] = new RayLine();
		}
	}

	public LayerIndex GetLayerIndex()
	{
		return layerIndex;
	}
	
// Etc
	public RayLine GetRayLine(int i)
	{
		return rayLines[i];
	}
	
	public int Length()
	{
		return rayLines.Length;
	}

// implements IRayed

	public void Ray(int index, float distance, float coverage, bool hit)
	{
		rayLines[index].length = distance;
		rayLines[index].hit = hit;
		rayLines[index].coverage = coverage;
	}

	public void RayParams(int index, Vector3 position, Vector3 direction)
	{
		rayLines[index].position = position;
		rayLines[index].direction = direction;
	}

	public enum LayerIndex
	{
		WALL = 0,
		PURSUER,
		PURSUEE,
		OTHER,
		LENGTH // Must end in LENGTH ( enum performance hack )
	}
}