﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentController : MonoBehaviour {

	private IGenerated[] environmentGenerators;
	private GameObject generatedRoot;
	private GameObject environmentRoot;

	public void Start()
	{
		environmentRoot = gameObject;
		generatedRoot = environmentRoot.transform.Find("Generated").gameObject;
		GameObject generatorsRoot = environmentRoot.transform.Find("Generators").gameObject;

		environmentGenerators = generatorsRoot.GetComponents<IGenerated>();
		foreach (IGenerated g in environmentGenerators)
		{
			g.SetRoot(generatedRoot);
		}
	}

	public void Generate()
	{
		if (!enabled) return;
		generatedRoot.RemoveChildren();

		foreach (IGenerated g in environmentGenerators)
		{
			if (g.GetEnabled())
			{
				g.Generate();
			}
		}
	}

	public IGenerated[] GetGenerators()
	{
		return environmentGenerators;
	}
}
