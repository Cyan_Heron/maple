﻿using MLAgents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
	private GameObject pursueeRoot;
	private GameObject pursuerRoot;
	private GameObject environmentRoot;

	private MAPLEPursuer[] pursuers;
	private MAPLEPursuee[] pursuees;
	private MAPLEAgent[] targetGroup;
	private EnvironmentController environmentController;

	private MAPLEAcademy academy;

	private bool trainingPursuersOrPursuee;
	//private bool resetAgentsOnDone;
	private bool resetEnvironmentOnDone;

	private bool finished;
	private int maxSteps;
	private int steps;

	private int id;

	// Use this for initialization
	void Start(){
		id = 0;
		finished = true;
		pursuerRoot = transform.Find("Pursuers").gameObject;
		pursueeRoot = transform.Find("Pursuees").gameObject;
		environmentRoot = transform.Find("Environment").gameObject;

		pursuers = pursuerRoot.GetComponentsInChildren<MAPLEPursuer>();
		pursuees = pursueeRoot.GetComponentsInChildren<MAPLEPursuee>();
		environmentController = environmentRoot.GetComponent<EnvironmentController>();
	}
	   
	public void Initialise(MAPLEAcademy academy, int maxStep, bool trainingPursuersOrPursuee, bool resetAgentsOnDone, bool resetEnvironmentOnDone) {
		this.academy = academy;
		this.trainingPursuersOrPursuee = trainingPursuersOrPursuee;
		//this.resetAgentsOnDone = resetAgentsOnDone;
		this.resetEnvironmentOnDone = resetEnvironmentOnDone;

		targetGroup = ((trainingPursuersOrPursuee) ? pursuers : (MAPLEAgent[])pursuees);
		maxSteps = maxStep;

		steps = 0;

		environmentController.Generate();
	}

	private void ResetEnvironment()
	{
		environmentController.Generate();
	}
	
	private void SetReward(float reward){
		foreach (MAPLEAgent a in targetGroup){
			a.SetReward(reward);
		}
	}

	private void AddReward(float reward){
		foreach (MAPLEAgent a in targetGroup)
		{
			a.AddReward(reward);
		}
	}

	private void Done()
	{
		foreach (MAPLEAgent a in pursuers)
		{
			if(!a.IsDone())
				a.Done();
		}
		foreach (MAPLEAgent a in pursuees)
		{
			if (!a.IsDone())
				a.Done();
		}

		if (resetEnvironmentOnDone)
		{
			ResetEnvironment();
		}

		id++;
		//Debug.Log("UpdateEnvHere" + id);
		steps = 0;
	}

	public void Win()
	{
		if (finished) return;  // Other agent Echo
		
		SetReward(0.5f);
		Finish(false);
	}

	public void Finish(bool forced)
	{
		if (finished) return;  // Other agent Echo
		finished = true;

		if (forced && steps < maxSteps){ // Incomplete Episode
			//Debug.Log("Rejected");
			steps = 0;
			return;
		}

		AddReward(0.5f*(1f-steps/maxSteps));
		Done();
	}

	public void Step()
	{
		steps++;
		if (steps >= maxSteps) Finish(false);
	}

	public void Restart()
	{
		finished = false;
	}


	public MAPLEPursuee[] GetPursuees()
	{
		return pursuees;
	}

	public MAPLEPursuer[] GetPursuers()
	{
		return pursuers;
	}

	public EnvironmentController GetEnvironmentController()
	{
		return environmentController;
	}
}
