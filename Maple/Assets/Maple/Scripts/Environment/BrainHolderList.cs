﻿using MLAgents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainHolderList{

	public Brain brain;
	public Brain alternateBrain;

	private int indicator;
	private List<BrainHolder> holderList;

	private MAPLEAgent mainAgent;

	public BrainHolderList(GameObject root, Brain brain, Brain alternateBrain)
	{
		this.brain = brain;
		this.alternateBrain = alternateBrain;

		indicator = 0;
		holderList = new List<BrainHolder>();
		for (int i = 0; i < root.transform.childCount; i++)
		{
			MAPLEAgent a = root.transform.GetChild(i).gameObject.GetComponent<MAPLEAgent>();
			holderList.Add(new BrainHolder(a, brain, alternateBrain));
		}
	}

	public void Recalc() // TODO if dynamically changing number of agents
	{
		indicator = 0;
	}

	public void Next()
	{
		Disable();
		indicator = (indicator+1)% holderList.Count;
		Enable();
	}

	public void Previous()
	{
		Disable();
		indicator = (indicator - 1 + holderList.Count) % holderList.Count;
		Enable();
	}

	public void Enable()
	{
		holderList[indicator].Alternate();
		mainAgent = holderList[indicator].GetAgent();
	}

	public void Disable()
	{
		holderList[indicator].Reset();
		mainAgent = null;
	}

	public MAPLEAgent GetAgent()
	{
		return mainAgent;
	}

}
