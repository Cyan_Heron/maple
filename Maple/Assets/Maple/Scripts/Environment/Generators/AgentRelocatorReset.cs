﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentRelocatorReset : IGenerated
{
	public GameObject rootPursuers;
	public GameObject rootPursuees;
	private int countOfPursuers;
	private int countOfPursuees;
	
	public override void Generate()
	{
		if (disabled) return;

		foreach (MAPLEAgent child in rootPursuees.GetComponentsInChildren<MAPLEAgent>())
		{
			child.transform.position = child.initialLocation;
		}
		foreach (MAPLEAgent child in rootPursuers.GetComponentsInChildren<MAPLEAgent>())
		{
			child.transform.position = child.initialLocation;
		}
	}
}
