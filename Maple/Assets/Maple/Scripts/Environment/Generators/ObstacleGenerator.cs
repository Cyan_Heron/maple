﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : IGenerated
{
	private System.Random rand;

	public GameObject obstaclePrefab;
	public int numberOfObstacles;

	protected override void Awake(){
		base.Awake();
		rand = new System.Random();
	}

	private double gaussian(float std)
	{
		double a = 1.0 - rand.NextDouble();
		double b = 1.0 - rand.NextDouble();
		double randStdNormal = Math.Sqrt(-2.0 * Math.Log(a)) *
					 Math.Sin(2.0 * Math.PI * b);
		return std * randStdNormal;
	}

	public override void Generate(){

		if (disabled) return;
			
		for (int i = 0; i < numberOfObstacles; i++){
			float size = (float)gaussian(10) * 0.25f;
			if (size < 0.25f) continue;
			if (size < 0.75f) size = 0.75f;

			float x = (float)gaussian(60) * 0.5f + 30f;
			float z = (float)gaussian(45) * 0.5f + 22.5f;
			if (x > 30) x -= 60f;
			if (z > 22.5) z -= 45f;
	
			GameObject gameObject = Instantiate(obstaclePrefab, new Vector3(x, 0, z), Quaternion.identity, root.transform);
			gameObject.transform.localScale = new Vector3(size, size, size);
		}
	}
}
