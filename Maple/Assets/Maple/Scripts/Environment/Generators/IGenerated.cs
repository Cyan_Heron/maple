﻿using UnityEngine;

public abstract class IGenerated : MonoBehaviour // pseudo Interface
{
	public abstract void Generate();
	protected bool disabled;
	protected GameObject root;

	protected virtual void Awake()
	{
		disabled = !enabled;
	}

	protected void OnEnable()
	{
		disabled = false;
	}
	protected void OnDisable()
	{
		disabled = true;
	}

	public bool GetEnabled()
	{
		return !disabled;
	}

	public void SetRoot(GameObject root)
	{
		this.root = root;
	}
}