﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGeneratorMaze : IGenerated
{

	public GameObject obstaclesRoot;
	public GameObject obstaclesBottom;
	public GameObject obstaclesBorder;
	public GameObject obstaclePrefab;
	public double fillRatio;  // coefficient specifying count of obstacle on map, 0 - empty map, 1 - max count of obstacles
	public int basicLength; // coefficient specifying distance between closest parallel obstacles (in basicUnitLength)
	private static float basicUnitLength = 1.0f;

	public struct obstacleMazeInformation
	{
		public int index;
		public float positionX;
		public float positionZ;
		public float sizeX;
		public float sizeZ;

		public obstacleMazeInformation(float positionX, float positionZ, float sizeX, float sizeZ, int index)
		{
			this.positionX = positionX * basicUnitLength;
			this.positionZ = positionZ * basicUnitLength;
			this.sizeX = sizeX * basicUnitLength;
			this.sizeZ = sizeZ * basicUnitLength;
			this.index = index;
		}
	}

	public enum Direction
	{
		North,
		NorthEast,
		East,
		SouthEast,
		South,
		SouhtWest,
		West,
		NorthWest
	}

	private void changeBottomLvl(int width, int length) //DONE 
	{
		int x = (int)obstaclesBottom.transform.localScale.x;
		int z = (int)obstaclesBottom.transform.localScale.z;

		obstaclesBottom.transform.localScale += new Vector3(width - x, 0, length - z);
	}

	private void adjustWallsToBotoomLvl(float width, float length) //DONE 
	{
		GameObject WallN = GameObject.Find("WallN");
		float oldWidth = (float)WallN.transform.localScale.x;

		GameObject WallE = GameObject.Find("WallE");
		float oldLength = (float)WallE.transform.localScale.z;

		float ratioWidth = width / oldWidth;
		float ratioLength = length / oldLength;

		float oldScaleX = (float)obstaclesBorder.transform.localScale.x;
		float oldScaleZ = (float)obstaclesBorder.transform.localScale.z;

		obstaclesBorder.transform.localScale += new Vector3(ratioWidth - oldScaleX, 0, ratioLength - oldScaleZ);
	}

	private List<obstacleMazeInformation> generateFullObstaclesInformations(int width, int length) //DOING 
	{
		List<obstacleMazeInformation> result = new List<obstacleMazeInformation>();

		int countWidth = (int)Math.Floor(width / (basicLength * basicUnitLength));
		int countLength = (int)Math.Floor(length / (basicLength * basicUnitLength));
		int maxIndexZ = (countLength - 1) / 2, currentIndex;
		int indexX = 0, indexZ = 0;
		double randS;

		//create full maze
		for (int x = -countWidth / 2 + 1; x < countWidth / 2 - 1; x++)
		{
			indexZ = 0;
			for (int z = -countLength / 2 + 1; z < countLength / 2 - 1; z++)
			{
				currentIndex = indexZ + maxIndexZ * indexX;
				if (indexX % 2 == indexZ % 2)
				{
					randS = UnityEngine.Random.value < 0.5f ? 1 : -1;
					if (randS >= 0.5)
					{
						result.Add(new obstacleMazeInformation(0 + basicLength * x, basicLength / 2 + basicLength * z, 1, basicLength, currentIndex * 4 + 0)); //E
						result.Add(new obstacleMazeInformation(basicLength / 2 + basicLength * x, 0 + basicLength * z, basicLength, 1, currentIndex * 4 + 1)); //S
					}
					else
					{
						result.Add(new obstacleMazeInformation(basicLength / 2 + basicLength * x, basicLength + basicLength * z, basicLength, 1, currentIndex * 4 + 2)); //N
						result.Add(new obstacleMazeInformation(basicLength + basicLength * x, basicLength / 2 + basicLength * z, 1, basicLength, currentIndex * 4 + 3)); //W
					}


				}
				indexZ++;
			}
			indexX++;
		}


		//make simple path



		return result;
	}

	private List<obstacleMazeInformation> generateTestObstaclesInformations(int width, int length) //DONE 
	{
		List<obstacleMazeInformation> result = new List<obstacleMazeInformation>();

		int maxIndexZ = 10, maxIndexX = 10;
		int indexX = 0, indexZ = 0, currentIndex;


		for (int x = 0; x < maxIndexX; x++)
		{
			indexZ = 0;
			for (int z = 0; z < maxIndexZ; z++)
			{
				currentIndex = indexZ + maxIndexZ * indexX;
				if (indexX % 2 == indexZ % 2)
				{
					result.Add(new obstacleMazeInformation(0 + basicLength * x, basicLength / 2 + basicLength * z, 1, basicLength, currentIndex * 4 + 0)); //E
					result.Add(new obstacleMazeInformation(basicLength / 2 + basicLength * x, 0 + basicLength * z, basicLength, 1, currentIndex * 4 + 1)); //S
					result.Add(new obstacleMazeInformation(basicLength / 2 + basicLength * x, basicLength + basicLength * z, basicLength, 1, currentIndex * 4 + 2)); //N
					result.Add(new obstacleMazeInformation(basicLength + basicLength * x, basicLength / 2 + basicLength * z, 1, basicLength, currentIndex * 4 + 3)); //W
				}
				indexZ++;
			}
			indexX++;
		}

		return result;
	}

	private bool isNextAreaInBoard(Direction direction, int maxLength, int maxWidth, int currentIndex) //DOING 
	{
		bool result = false;
		switch (direction)
		{
			case Direction.North:
				if (currentIndex % maxLength == 0) result = false;
				else result = true;

				break;
			case Direction.NorthEast:
				if ((currentIndex % maxLength == 0) || (currentIndex < maxLength)) result = false;
				else result = true;

				break;
			case Direction.East:
				if (currentIndex < maxLength) result = false;
				else result = true;

				break;
			case Direction.SouthEast:
				if ((currentIndex % maxLength == maxLength - 1) || (currentIndex < maxLength)) result = false;
				else result = true;

				break;
			case Direction.South:
				if (currentIndex % maxLength == maxLength - 1) result = false;
				else result = true;

				break;
			case Direction.SouhtWest:
				if ((currentIndex % maxLength == maxLength - 1) || (currentIndex >= maxLength * (maxWidth - 1))) result = false;
				else result = true;

				break;
			case Direction.West:
				if (currentIndex >= maxLength * (maxWidth - 1)) result = false;
				else result = true;

				break;
			case Direction.NorthWest:
				if ((currentIndex % maxLength == 0) || (currentIndex >= maxLength * (maxWidth - 1))) result = false;
				else result = true;

				break;
			default:

				break;
		}

		return result;

	}

	private int calulateNextAreaInMaze(Direction direction, int maxLength, int currentIndex) //DONE 
	{
		int result = -1;
		switch (direction)
		{
			case Direction.North:
				result -= 1;     //as N

				break;
			case Direction.NorthEast: //N-E
				result -= 1;         //as N
				result += maxLength; //as E

				break;
			case Direction.East:
				result += maxLength; //as E

				break;
			case Direction.SouthEast:
				result += 1;         //as S
				result += maxLength; //as E

				break;
			case Direction.South:
				result += 1; //as S

				break;
			case Direction.SouhtWest:
				result += 1;         //as S
				result -= maxLength; //as W

				break;
			case Direction.West:
				result -= maxLength; //as W

				break;
			case Direction.NorthWest:
				result -= 1;         //as N
				result -= maxLength; //as W

				break;
			default:

				break;
		}


		return result;
	}

	private List<obstacleMazeInformation> generateBasicTunnels(List<obstacleMazeInformation> inputList) //TO DO 
	{
		List<obstacleMazeInformation> result = new List<obstacleMazeInformation>();


		return result;
	}

	private List<obstacleMazeInformation> shuffleList(List<obstacleMazeInformation> inputList) //DONE 
	{
		List<obstacleMazeInformation> randomList = new List<obstacleMazeInformation>();

		System.Random r = new System.Random();
		int randomIndex = 0;
		while (inputList.Count > 0)
		{
			randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
			randomList.Add(inputList[randomIndex]); //add it to the new, random list
			inputList.RemoveAt(randomIndex); //remove to avoid duplicates
		}

		return randomList; //return the new random list
	}

	private List<obstacleMazeInformation> includeFillRatio(List<obstacleMazeInformation> orgList, float fillRatio) // DONE
	{
		//move all to "result", in random order
		List<obstacleMazeInformation> result = shuffleList(orgList);


		int countOfElements = result.Count, limitIndex = -1;
		float[] prefixSumOfField = new float[countOfElements];
		float previusSum = 0, limitSum = 0;

		//calc prefix sum from the field
		for (int i = 0; i < countOfElements; i++)
		{
			prefixSumOfField[i] = previusSum + result[i].sizeX * result[i].sizeZ;
			previusSum = prefixSumOfField[i];
		}

		limitSum = prefixSumOfField[countOfElements - 1] * fillRatio;

		//find limit index
		for (int i = 0; i < countOfElements; i++)
		{
			if (prefixSumOfField[i] > limitSum)
			{
				limitIndex = i;
				break;
			}
		}

		if (limitIndex == -1)
			limitIndex = countOfElements - 1;

		//delete the right amount of obstacles
		if (limitSum != countOfElements - 1)
		{
			result.RemoveRange(limitIndex + 1, countOfElements - limitIndex - 1);
		}


		return result;
	}

	private void createObstaclesOnMap(List<obstacleMazeInformation> list) //DONE
	{
		foreach (obstacleMazeInformation inf in list)
		{
			GameObject gameObject = Instantiate(obstaclePrefab, new Vector3(inf.positionX, 0, inf.positionZ), Quaternion.identity, obstaclesRoot.transform);
			gameObject.transform.localScale = new Vector3(inf.sizeX, 2.0f, inf.sizeZ);
		}
	}

	public override void Generate() //DOING 
	{
		//check if map is disabled
		if (disabled) return;

		int width = 60;
		int length = 45;

		//create floor about given sizes
		changeBottomLvl(width, length);

		//match the walls to the floor
		adjustWallsToBotoomLvl((float)width, (float)length);

		List<obstacleMazeInformation> obsInfList;

		//create full maze
		obsInfList = generateFullObstaclesInformations(width, length);

		//create test maze
		//obsInfList = generateTestObstaclesInformations(width, length);

		//generate basic maze with some tunnels
		//obsInfList = generateBasicTunnels(obsInfList);

		//include user fill ratio about obstacles on board
		obsInfList = includeFillRatio(obsInfList, (float)fillRatio);

		//create obstacles to map
		createObstaclesOnMap(obsInfList);
	}
}
