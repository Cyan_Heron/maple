﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentRelocator : IGenerated
{
	private System.Random rand;

	public GameObject rootPursuers;
	public GameObject rootPursuees;
	public double focusFactorPursuers; // coefficient specifying field to spawn pursuers, 0 - big field, 1 - small field
	public double focusFactorPursuees; // coefficient specifying field to spawn pursuees, 0 - big field, 1 - small field
	public double distanceFactor; //coefficient specifying distance between two generators, 0 - big distance, 1 - small distance
	private int countOfPursuers;
	private int countOfPursuees;
	private float percentageOfUsedField = 0.95f;
	private int width;
	private int length;

	public struct genPurInformation
	{
		public float positionX;
		public float positionZ;
		public float focusFactor;

		public genPurInformation(float positionX, float positionZ, float focusFactor)
		{
			this.positionX = positionX;
			this.positionZ = positionZ;
			this.focusFactor = focusFactor;
		}
	}

	public struct purInformation
	{
		public int index;
		public float positionX;
		public float positionZ;

		public purInformation(float positionX, float positionZ, int index)
		{
			this.positionX = positionX;
			this.positionZ = positionZ;
			this.index = index;
		}
	}

	private static double floatToDouble(float value) //DONE 
	{
		decimal dec = new decimal(value);
		return (double)dec;
	}

	private static float doubleToFloat(double value) //DONE 
	{
		return (float)value;
	}

	private float generateRandomValueBetween(float min, float max) //DONE 
	{
		double dMax = floatToDouble(max);
		double dMin = floatToDouble(min);
		double dResult = rand.NextDouble() * (dMax - dMin) + dMin;
		return doubleToFloat(dResult);
	}

	private List<purInformation> genereteFullPurInformations(genPurInformation gen, int width, int length, int count)  //DONE, TO TEST 
	{
		List<purInformation> result = new List<purInformation>();

		//printfGenPurInformation(gen);

		float newX, newZ;
		float leftMaxX, rightMaxX, leftMaxZ, rightMaxZ;
		float resultOfFocusFactor;
		for (int i = 0; i < count; i++)
		{

			//determine the max distance to wall
			leftMaxX = width / 2 + gen.positionX;
			rightMaxX = width / 2 - gen.positionX;

			leftMaxZ = length / 2 + gen.positionZ;
			rightMaxZ = length / 2 - gen.positionZ;


			//calc result of focus factor
			resultOfFocusFactor = (0.9f - doubleToFloat(System.Math.Sqrt(gen.focusFactor)) * 0.8f);

			//generate random values
			newX = generateRandomValueBetween(-leftMaxX * resultOfFocusFactor + gen.positionX, rightMaxX * resultOfFocusFactor + gen.positionX);
			newZ = generateRandomValueBetween(-leftMaxZ * resultOfFocusFactor + gen.positionZ, rightMaxZ * resultOfFocusFactor + gen.positionZ);

			//print("nX: " + newX + " nZ " + newZ + "\n");

			//add info to list
			result.Add(new purInformation(newX, newZ, i));
		}


		return result;
	}

	private void chanePostitionOnMap(List<purInformation> list, GameObject currentPur) // DONE 
	{
		int i = 0;
		foreach (Transform child in currentPur.transform)
		{
			child.position = new Vector3(list[i].positionX, 0.0f, list[i].positionZ);
			//print(child.position.x + " " + child.position.z + "\n");
			//print(i + " " + list[i].positionX + " " + list[i].positionZ + "\n");

			i++;
		}
	}

	private int countOfChilderns(GameObject go) // DONE 
	{
		int result = 0;

		foreach (Transform child in go.transform)
		{
			result++;
		}

		return result;
	}

	private int findWidth(GameObject go) // DONE 
	{
		// TODO To nie jest dobry pomysł nie wszystkie generatory muszą być aktywne
		int result = -1;
		IGenerated[] environmentGenerators;

		environmentGenerators = go.GetComponents<IGenerated>();

		foreach (IGenerated g in environmentGenerators)
		{
			if (g.GetEnabled())
			{
				result = width;
			}
		}

		return result;
	}

	private int findLength(GameObject go) // DONE 
	{
		// TODO To nie jest dobry pomysł nie wszystkie generatory muszą być aktywne
		int result = -1;
		IGenerated[] environmentGenerators;

		environmentGenerators = go.GetComponents<IGenerated>();

		foreach (IGenerated g in environmentGenerators)
		{
			if (g.GetEnabled())
			{
				result = length;
			}
		}
		return result;
	}

	private genPurInformation generatePurGenerator(genPurInformation anotherGen, double distanceFactor, double focusFactor, int length, int width) // DOING 
	{
		genPurInformation result;
		float x, z;
		float leftMaxX, rightMaxX, leftMaxZ, rightMaxZ;
		float factorOfUsage = 0.8f;

		if (distanceFactor == -1.0)
		{
			x = generateRandomValueBetween(-width / 2 * this.percentageOfUsedField, width / 2 * this.percentageOfUsedField);
			z = generateRandomValueBetween(-length / 2 * this.percentageOfUsedField, length / 2 * this.percentageOfUsedField);
			result = new genPurInformation(x, z, doubleToFloat(focusFactor));
		}
		else
		{
			//determine the max distance to wall
			leftMaxX = width / 2 + anotherGen.positionX;
			rightMaxX = width / 2 - anotherGen.positionX;

			leftMaxZ = length / 2 + anotherGen.positionZ;
			rightMaxZ = length / 2 - anotherGen.positionZ;

			float resultOfDistanceFactor = (0.9f - doubleToFloat(distanceFactor) * factorOfUsage);

			if (leftMaxX > rightMaxX)
			{
				x = anotherGen.positionX - leftMaxX * resultOfDistanceFactor;
			}
			else
			{
				x = anotherGen.positionX + rightMaxX * resultOfDistanceFactor;
			}

			if (leftMaxZ > rightMaxZ)
			{
				z = anotherGen.positionZ - leftMaxZ * resultOfDistanceFactor;

			}
			else
			{
				z = anotherGen.positionZ + rightMaxZ * resultOfDistanceFactor;
			}

			result = new genPurInformation(x, z, doubleToFloat(focusFactor));
		}

		return result;

	}

	private void printfGenPurInformation(genPurInformation gen) // DONE 
	{
		print("x: " + gen.positionX + "\n");
		print("z: " + gen.positionZ + "\n");
		print("fo fa: " + gen.focusFactor + "\n");
	}

	private void printfPurInformation(purInformation pu) // DONE
	{
		print("indx: " + pu.index + "\n");
		print("x: " + pu.positionX + "\n");
		print("z: " + pu.positionZ + "\n");
	}

	private void printfListOfPurInformations(List<purInformation> list) // DONE
	{
		foreach (purInformation ele in list)
		{
			printfPurInformation(ele);
		}
	}


	public override void Generate()
	{
		if (disabled) return;

		this.rand = new System.Random();

		//count the number of pursuers
		this.countOfPursuers = countOfChilderns(rootPursuers); //Tested 100%

		//print(this.countOfPursuers);

		//count the number of pursuees
		this.countOfPursuees = countOfChilderns(rootPursuees); // Tested 100%

		//print(this.countOfPursuees);

		//find the length of the map
		this.length = 45;//Zbyt kosztowne wyjaśnione w metodzie:   findLength(GameObject.Find("Generators")); //Tested 100%

		//print(this.length);

		//find the width of the map
		this.width = 60;// Zbyt kosztowne wyjaśnione w metodzie:   findWidth(GameObject.Find("Generators")); //Tested 100%

		//print(this.width);

		List<purInformation> puerInfList, pueeInfList;
		genPurInformation genPursuer, genPursuee, tmp = new genPurInformation();

		//create generator to pursuers
		genPursuer = generatePurGenerator(tmp, -1.0, this.focusFactorPursuers, this.length, this.width); //Tested 100 %

		//printfGenPurInformation(genPursuer);

		//create generator to pursuees
		genPursuee = generatePurGenerator(genPursuer, this.distanceFactor, this.focusFactorPursuees, this.length, this.width); //Tested 100 %

		//printfGenPurInformation(genPursuee);

		//generate count of pursuers on random position
		puerInfList = genereteFullPurInformations(genPursuer, this.width, this.length, this.countOfPursuers); //Tested 100 %

		//printfGenPurInformation(genPursuer);
		//printfListOfPurInformations(puerInfList);

		//generate count of pursuees on random position
		pueeInfList = genereteFullPurInformations(genPursuee, this.width, this.length, this.countOfPursuees); //Tested 100 %

		//printfGenPurInformation(genPursuee);
		//printfListOfPurInformations(pueeInfList);

		//change positions pursuers to map
		chanePostitionOnMap(puerInfList, rootPursuers); //Not sure, but tested

		//change positions pursuees to map
		chanePostitionOnMap(pueeInfList, rootPursuees); //Not sure, but tested

	}
}
