﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGeneratorBall : IGenerated
{
	private System.Random rand;

	public GameObject obstaclesRoot;
	public GameObject obstaclesBottom;
	public GameObject obstaclesBorder;
	public GameObject obstaclePrefab;
	public double fillRatio; // coefficient specifying count of obstacle on map, 0 - empty map, 1 - max count of obstacles
	private float percentageOfUsedField = 0.9f;

	public struct obstacleBallInformation
	{
		public float x;
		public float z;
		public float r;
		public int index;

		public obstacleBallInformation(float x, float z, float r, int index)
		{
			this.x = x;
			this.z = z;
			this.r = r;
			this.index = index;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		rand = new System.Random();
	}

	private double gaussian(float std) //DONE 
	{
		double a = 1.0 - rand.NextDouble();
		double b = 1.0 - rand.NextDouble();
		double randStdNormal = Math.Sqrt(-2.0 * Math.Log(a)) *
					 Math.Sin(2.0 * Math.PI * b);
		return std * randStdNormal;
	}

	private void printObstacle(obstacleBallInformation obs) //DONE 
	{
		print("x: " + obs.x + " z: " + obs.z + " r: " + obs.r + " index: " + obs.index);
	}

	private void printAllObstacles(List<obstacleBallInformation> list) //DONE 
	{
		foreach (obstacleBallInformation obs in list)
		{
			printObstacle(obs);
		}
	}

	private void changeBottomLvl(int width, int length) //DONE 
	{
		int x = (int)obstaclesBottom.transform.localScale.x;
		int z = (int)obstaclesBottom.transform.localScale.z;

		obstaclesBottom.transform.localScale += new Vector3(width - x, 0, length - z);
	}

	private void adjustWallsToBotoomLvl(float width, float length) //DONE 
	{
		GameObject WallN = GameObject.Find("WallN");
		float oldWidth = (float)WallN.transform.localScale.x;

		GameObject WallE = GameObject.Find("WallE");
		float oldLength = (float)WallE.transform.localScale.z;

		float ratioWidth = width / oldWidth;
		float ratioLength = length / oldLength;

		float oldScaleX = (float)obstaclesBorder.transform.localScale.x;
		float oldScaleZ = (float)obstaclesBorder.transform.localScale.z;

		obstaclesBorder.transform.localScale += new Vector3(ratioWidth - oldScaleX, 0, ratioLength - oldScaleZ);
	}

	private static double floatToDouble(float value) //DONE 
	{
		decimal dec = new decimal(value);
		return (double)dec;
	}

	private static float doubleToFloat(double value) //DONE 
	{
		return (float)value;
	}

	private float generateRandomValueBetween(float min, float max) //DONE 
	{
		double dMax = floatToDouble(max);
		double dMin = floatToDouble(min);
		double dResult = rand.NextDouble() * (dMax - dMin) + dMin;
		return doubleToFloat(dResult);
	}

	private List<obstacleBallInformation> generateNewObstaclesInformations(int width, int length) //DONE, TO MODIFY 
	{
		List<obstacleBallInformation> result = new List<obstacleBallInformation>();

		//parameters to modify
		int constantArea = 100; // j^2
		int constantObstacles = 2; //how many obstacles on constantArea (j^2)
		float minObstacleSize = 1.0f;
		float maxObstacleSize = 4.0f;

		int numberOfObstacles = width * length * constantObstacles / constantArea;

		float newX, newZ, newSize;
		for (int i = 0; i < numberOfObstacles; i++)
		{
			//generate random values
			newSize = generateRandomValueBetween(minObstacleSize, maxObstacleSize);
			newX = generateRandomValueBetween(-0.5f * width * this.percentageOfUsedField + 2 * newSize, 0.5f * width * this.percentageOfUsedField - 2 * newSize);
			newZ = generateRandomValueBetween(-0.5f * length * this.percentageOfUsedField, 0.5f * length * this.percentageOfUsedField);


			//add info to list
			result.Add(new obstacleBallInformation(newX, newZ, newSize, i));
		}

		return result;
	}

	private List<obstacleBallInformation> generateTestObstaclesInformations() //DONE, TO MODIFY 
	{
		float size = 1.0f;

		List<obstacleBallInformation> result = new List<obstacleBallInformation>();

		result.Add(new obstacleBallInformation(size, size, size, 0)); ;
		result.Add(new obstacleBallInformation(size / 2, size / 2, size, 2));
		result.Add(new obstacleBallInformation(2 * size, 2 * size, size / 2, 1));
		result.Add(new obstacleBallInformation(4 * size, 4 * size, 2 * size, 3));
		result.Add(new obstacleBallInformation(3 * size, 3 * size, size / 2, 4));

		return result;
	}

	public static float distanceTwoPoints(float x1, float y1, float x2, float y2) //DONE 
	{
		double x1d = floatToDouble(x1);
		double y1d = floatToDouble(y1);
		double x2d = floatToDouble(x2);
		double y2d = floatToDouble(y2);

		return (float)Math.Sqrt((x1d - x2d) * (x1d - x2d) + (y1d - y2d) * (y1d - y2d));
	}

	public static float distanceToBoardCenter(obstacleBallInformation obj) //DONE 
	{
		float x = obj.x;
		float z = obj.z;

		return distanceTwoPoints(x, z, 0.0f, 0.0f);
	}

	private static int closerToBoardCenter(obstacleBallInformation obj1, obstacleBallInformation obj2)
	{
		if (distanceToBoardCenter(obj1) > distanceToBoardCenter(obj2))
			return 1;
		else if (distanceToBoardCenter(obj1) < distanceToBoardCenter(obj2))
			return -1;
		else return 0;
	}

	private bool haveObstaclesCollision(obstacleBallInformation objFirst, obstacleBallInformation objSecond) //DONE 
	{
		float firstX = objFirst.x;
		float firstZ = objFirst.z;
		float firstR = objFirst.r;

		float seconX = objSecond.x;
		float seconZ = objSecond.z;
		float seconR = objSecond.r;

		float distTwoCenters = distanceTwoPoints(firstX, firstZ, seconX, seconZ);
		float sumOfRadius = firstR + seconR;

		if (distTwoCenters > sumOfRadius)
			return false;
		else
			return true;
	}

	private List<obstacleBallInformation> shuffleList(List<obstacleBallInformation> inputList) //DONE 
	{
		List<obstacleBallInformation> randomList = new List<obstacleBallInformation>();

		System.Random r = new System.Random();
		int randomIndex = 0;
		while (inputList.Count > 0)
		{
			randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
			randomList.Add(inputList[randomIndex]); //add it to the new, random list
			inputList.RemoveAt(randomIndex); //remove to avoid duplicates
		}

		return randomList; //return the new random list
	}

	private List<obstacleBallInformation> includeFillRatio(List<obstacleBallInformation> orgList, float fillRatio) // DONE 
	{
		//move all to "result", in random order
		List<obstacleBallInformation> result = shuffleList(orgList);

		int countOfElements = result.Count, limitIndex = -1;
		float[] prefixSumOfField = new float[countOfElements];
		float previusSum = 0, limitSum;


		//calc prefix sum from the field
		for (int i = 0; i < countOfElements; i++)
		{
			prefixSumOfField[i] = previusSum + result[i].r * result[i].r;
			previusSum = prefixSumOfField[i];
		}

		limitSum = prefixSumOfField[countOfElements - 1] * fillRatio;

		//find limit index
		for (int i = 0; i < countOfElements; i++)
		{
			if (prefixSumOfField[i] > limitSum)
			{
				limitIndex = i;
				break;
			}
		}

		if (limitIndex == -1)
			limitIndex = countOfElements - 1;

		//print(limitIndex);

		//delete the right amount of obstacles
		if (limitSum != countOfElements - 1)
		{
			result.RemoveRange(limitIndex + 1, countOfElements - limitIndex - 1);
		}

		return result;
	}

	private void createObstaclesOnMap(List<obstacleBallInformation> list) //DONE 
	{
		foreach (obstacleBallInformation inf in list)
		{
			GameObject gameObject = Instantiate(obstaclePrefab, new Vector3(inf.x, 0, inf.z), Quaternion.identity, obstaclesRoot.transform);
			gameObject.transform.localScale = new Vector3(inf.r, inf.r, inf.r);
		}
	}

	private List<obstacleBallInformation> movingObstacles(List<obstacleBallInformation> orgList)  // DONE
	{
		List<obstacleBallInformation> result = new List<obstacleBallInformation>();

		bool hasConflicts;
		int countOfObstacles = orgList.Count;
		int countOfResultObtacles;

		for (int i = 0; i < countOfObstacles; i++)
		{
			/*do // TODO To jest do poprawy. Robienie while to jest zły pomysł bo nie ma on okreslonego czasu wykonania
			{
				hasConflicts = false;
				countOfResultObtacles = result.Count;
				for (int j = 0; j < countOfResultObtacles; j++)
				{
					if (haveObstaclesCollision(orgList[i], result[j])) hasConflicts = true;

				}

				
				// TODO Width i Length powinien być pobierany ze środowiska
				/*
				if (hasConflicts)
				{
					//generete new coorinates of obstacles
					float newX = generateRandomValueBetween(-width / 2 * this.percentageOfUsedField, width / 2 * this.percentageOfUsedField);
					float newZ = generateRandomValueBetween(-length / 2 * this.percentageOfUsedField, length / 2 * this.percentageOfUsedField);

					//repleace old information for new informations
					obstacleBallInformation tmp = new obstacleBallInformation(newX, newZ, orgList[i].r, orgList[i].index);
					orgList.RemoveAt(i);
					orgList.Insert(i, tmp);

				}
				

		} while (hasConflicts) ; */

			//if (!hasConflicts)
				result.Add(orgList[i]);
		}


		return result;
	}

	public override void Generate() //DOING 
	{
		int width = 60;
		int length = 45;

		//check if map is disabled
		if (disabled) return;

		//create floor about given sizes
		changeBottomLvl(width, length);

		//match the walls to the floor
		adjustWallsToBotoomLvl((float)width, (float)length);

		List<obstacleBallInformation> obsInfList;

		//create new random obstacles
		obsInfList = generateNewObstaclesInformations(width, length);

		//create test obstacles
		//obsInfList = generateTestObstaclesInformations();

		//sort all obstacles in order distance to center of board
		obsInfList.Sort((x, y) => closerToBoardCenter(x, y));

		//include user fill ratio about obstacles on board
		obsInfList = includeFillRatio(obsInfList, (float)fillRatio);

		//moving obstacles to abolish collisions of obstacles
		obsInfList = movingObstacles(obsInfList);

		//create obstacles to map
		createObstaclesOnMap(obsInfList);
	}
}
