﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollidingMovement : MonoBehaviour
{
	public int numberOfRays;
	public int collisionLayer;

	private float size;
	private bool disabled;
	private float movementLimit = 0.005f;
	private float collidingRayOffset = 0.005f;

	private void Awake(){
		size = gameObject.transform.localScale.x;
		disabled = !enabled;
	}


	private Vector3 bounceIteration(Vector3 position, Vector3 direction, float distance, Vector3 thrust)
	{
		bool collisionTriggered = false;
		RaycastHit collisionHitClose = new RaycastHit(); // collisionHitClose.distance is without offset
		float collisionDistance = 0f;
		Vector3 posPrep = Vector3.zero;
		Vector3 posPrepOff = Vector3.zero;

		{ // Search for closest collision
			float collisionClose = float.MaxValue;
			RaycastHit rh;
			Vector3 dirPrep = MathUtils.RotateVector3(direction, 90);
			int layerMask = 1 << collisionLayer;
			float offset = 0.1f;
			float cro = collidingRayOffset;

			for (int i = 0; i < numberOfRays; i++)
			{
				float perc = (float)((float)i /(numberOfRays - 1)) * (1f + 2*cro) - cro - 0.5f;
				float percP2 = 0.25f - perc * perc;
				float rperc = (percP2 > 0)? Mathf.Sqrt(percP2) : 0f;
				Vector3 clinePosition =
						position
						+ dirPrep * (perc);
						//+ direction * (rperc); // crescent outer
				Vector3 clinePositionOffseted = clinePosition - direction * offset;

				if (Physics.Raycast(clinePositionOffseted, direction.normalized, out rh, rperc + distance + offset, layerMask))
				{
					if (collisionClose > rh.distance - rperc)
					{
						collisionClose = rh.distance - rperc;
						collisionDistance = rh.distance - offset - rperc;
						collisionHitClose = rh;
						collisionTriggered = true;

						posPrep = clinePosition;
						posPrepOff = clinePositionOffseted;
					}
				}
				Debug.DrawRay(
					clinePositionOffseted
					+ new Vector3(0f, 0.75f, 0f),
					direction.normalized * (rperc + distance + offset),
					Color.red);
			}
			if (collisionTriggered)Debug.DrawRay(posPrep, direction * 1f,Color.blue);
		}

		if (!collisionTriggered)
		{
			return direction * distance;
		}
		else
		{
			Debug.DrawRay(
				posPrep + direction * collisionDistance,
				collisionHitClose.normal,
				Color.white);

			float delta = 0.005f; 
			float collisionStop = Mathf.Max(0, collisionDistance - delta);
			float collisionBounce = Mathf.Max(0, distance - collisionStop - 2 * delta);

			Vector3 slope = collisionHitClose.normal;
			float angle = Vector3.SignedAngle(-direction, slope, Vector3.up);
	
			Vector3 deviation = MathUtils.RotateVector3(slope, (angle < 0) ? 90 : -90);

			Debug.DrawRay(
				position,
				deviation,
				Color.green);

			Vector3 result = direction * collisionStop;

			float angleLoss = Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * angle));

			float bounceSpeed = collisionBounce * angleLoss * angleLoss;
			if (Vector3.Angle(thrust, deviation) < 90f && bounceSpeed > movementLimit)
			{
				result += bounceIteration(position + result, deviation, bounceSpeed, direction);
			}
			return result;
		}
	}

	public Vector3 getBounce(float speed)
	{
		if (disabled) return new Vector3(0,0,speed);

		Vector3 ray = transform.TransformDirection(Vector3.forward).normalized;

		Vector3 translation = bounceIteration(transform.position, ray, speed, ray);
		Debug.DrawRay(
			transform.position,
			translation * 10f,
			Color.cyan);

		Vector3 result = transform.InverseTransformDirection(translation);

		return result;
	}

	private void OnEnable()
	{
		disabled = false;
	}

	private void OnDisable()
	{
		disabled = true;
	}

}
