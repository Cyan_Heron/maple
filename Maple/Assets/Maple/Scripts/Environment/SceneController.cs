﻿using MLAgents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {  

	public MAPLEAcademy academy;
	public int maxStep;

	public StageController mainStage;

	public GameObject stageRoot;
	[HideInInspector]
	public StageController[] stages;

	public bool trainingPursuersOrPursuee;
	public bool resetAgentsOnDone;
	public bool resetEnvironmentOnDone;

	private BrainController bc;
	
	// Use this for initialization
	void Start () {
		stages = stageRoot.GetComponentsInChildren<StageController>();
		bc = GetComponent<BrainController>();
		InitializeStages();

		if(bc!=null)bc.ApplyStage(mainStage, maxStep);
	}

	public void InitializeStages()
	{
		academy.SetAcademyState(this);
		foreach (StageController s in stages)
		{
			s.Initialise(academy, maxStep, trainingPursuersOrPursuee, resetAgentsOnDone, resetEnvironmentOnDone);
		}
	}

	public MAPLEAgent AccessAgent()
	{
		if (bc != null && bc.enabled) return bc.mainAgent;
		else return null;
	}
}
