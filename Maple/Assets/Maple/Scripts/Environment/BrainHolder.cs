﻿using MLAgents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainHolder{
	public MAPLEAgent agent;
	public Brain brain;
	public Brain targetBrain;
	public Brain alternateBrain;

	public BrainHolder(MAPLEAgent agent, Brain targetBrain, Brain alternateBrain)
	{
		this.agent = agent;
		this.targetBrain = targetBrain;
		this.alternateBrain = alternateBrain;

		Reset();
	}

	public void Alternate()
	{
		if (brain != alternateBrain)
		{
			brain = alternateBrain;
			agent.GiveBrain(brain);
		}
	}

	public void Reset()
	{
		if (brain != targetBrain)
		{
			brain = targetBrain;
			agent.GiveBrain(brain);
		}
	}


	public Brain GetBrain()
	{
		return brain;
	}

	public MAPLEAgent GetAgent()
	{
		return agent;
	}

	
}
