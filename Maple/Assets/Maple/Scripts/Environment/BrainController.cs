﻿using MLAgents;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainController : MonoBehaviour { // Set 

	private bool agentMod;

	public bool playing;
	public bool playingPursuerOrPursuee;

	public Brain pursuerBrain;
	public Brain pursueeBrain;
	public Brain pursuerPlayingBrain;
	public Brain pursueePlayingBrain;

	private GameObject pursueeRoot;
	private GameObject pursuerRoot;

	private BrainHolderList pursueeBrainsHolder;
	private BrainHolderList pursuerBrainsHolder;
	private BrainHolderList holder;

	[HideInInspector]
	public MAPLEAgent mainAgent;
	
	public void ApplyStage(StageController mainStage, int maxStep) {
		pursuerRoot = mainStage.transform.Find("Pursuers").gameObject;
		pursueeRoot = mainStage.transform.Find("Pursuees").gameObject;

		if (!enabled){ // Control without BrainHolderList for time optymalisation
			foreach (MAPLEAgent agent in pursueeRoot.GetComponentsInChildren<MAPLEAgent>()){
				agent.GiveBrain(pursueeBrain);
				agent.agentParameters.maxStep = maxStep;
			}
			foreach (MAPLEAgent agent in pursuerRoot.GetComponentsInChildren<MAPLEAgent>()){
				agent.GiveBrain(pursuerBrain);
				agent.agentParameters.maxStep = maxStep;
			}
			return;
		}

		pursuerBrainsHolder = new BrainHolderList(pursuerRoot, pursuerBrain, pursuerPlayingBrain);
		pursueeBrainsHolder = new BrainHolderList(pursueeRoot, pursueeBrain, pursueePlayingBrain);

		holder = (playingPursuerOrPursuee)? pursuerBrainsHolder : pursueeBrainsHolder;
		if (playing) holder.Enable();
		else holder.Disable();
		mainAgent = holder.GetAgent();
	}

	// Update is called once per frame
	void Update () {
		if (!enabled) return;

		agentMod = false;
		if (Input.GetKeyUp(KeyCode.Alpha1))
		{
			holder.Previous();
			agentMod = true;
		}
		if (Input.GetKeyUp(KeyCode.Alpha4))
		{
			holder.Next();
			agentMod = true;
		}
		if (Input.GetKeyUp(KeyCode.Alpha8))
		{
			playingPursuerOrPursuee = !playingPursuerOrPursuee;
			holder.Disable();
			holder = (playingPursuerOrPursuee) ? pursuerBrainsHolder : pursueeBrainsHolder;
			holder.Enable();
			agentMod = true;
		}
		if (Input.GetKeyUp(KeyCode.Alpha0))
		{
			playing = !playing;
			if (playing) holder.Enable();
			else holder.Disable();
			agentMod = true;
		}

		//---
		if (agentMod)
		{
			mainAgent = holder.GetAgent();
		}
	}
}
