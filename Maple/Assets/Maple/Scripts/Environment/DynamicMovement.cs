﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicMovement : MonoBehaviour
{
	public float agentLimitVelocity;
	public float agentLimitAcceleration;
	public float agentLimitDecelerationScale;
	public float agentLimitAgility;
	public float agentMovementScale;

	private float agentVelocity;
	private float agentAngularLoss;


	private CollidingMovement cm;

	private void Awake(){
		reset();
		cm = gameObject.GetComponent<CollidingMovement>();
	}

	private void calcAngularLoss(float angle)
	{
		agentAngularLoss = Mathf.Pow(Mathf.Cos(angle * Mathf.Deg2Rad * 4f), 0.5f);
	}

	public void rotate(float value) // value <-1,1>
	{
		float angle = value * agentLimitAgility;
		gameObject.transform.Rotate(new Vector3(0, angle, 0));

		calcAngularLoss(angle);
	}

	public float rotateTowards(Vector3 value) // returns angle of rotation
	{
		float direction = gameObject.transform.rotation.eulerAngles.y;
		float target = Vector3.SignedAngle(Vector3.forward,value,Vector3.up);

		float angle = target - direction;
		if (angle > 180) angle = angle - 360;
		if (angle < -180) angle = angle + 360;

		if (angle > 0) angle = Mathf.Min(agentLimitAgility, angle);
		else if(angle < 0) angle = Mathf.Max(-agentLimitAgility, angle);

		gameObject.transform.Rotate(new Vector3(0, angle, 0));
		calcAngularLoss(angle);
		return angle;
	}

	public void accelerate(float value) // value <0,1>
	{
		if (value > agentVelocity) {
			agentVelocity = Mathf.Min(agentVelocity + agentLimitAcceleration, value);
		}
		else if (value < agentVelocity)
		{
			agentVelocity = Mathf.Max(agentVelocity - agentLimitAcceleration*agentLimitDecelerationScale, value);
		}
		else {
			// Empty
		}
		agentVelocity = Mathf.Pow(agentVelocity, agentAngularLoss) * Mathf.Pow(1/4f,1 - agentAngularLoss); // 1/4 - im większe tym szybsze

		float speed = (float)MathUtils.BetaFunction(agentVelocity, 0.75, 4.0)
				* agentLimitVelocity;

		// Scaling factor
		speed = speed * agentMovementScale;

		// Rebounce
		Vector3 collidedBounce = cm.getBounce(speed);
		gameObject.transform.Translate(collidedBounce);

		float angularMomentumLoss = Vector3.Angle(Vector3.forward, collidedBounce);
		agentVelocity *= Mathf.Pow(Mathf.Cos(Mathf.Deg2Rad * angularMomentumLoss), 0.25f);
		
	}

	public void reset()
	{
		agentAngularLoss = 0;
		agentVelocity = 0;
	}

	internal float getVelocity()
	{
		return agentVelocity;
	}

	internal float getAngularLoss()
	{
		return agentAngularLoss;
	}

	internal Vector3 getDirection()
	{
		return MathUtils.RotateVector3(
			new Vector3(0, 0, 1),
			-gameObject.transform.rotation.eulerAngles.y);
	}
}
