﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class HierarchySkip : MonoBehaviour {
	// Its a hack that prevents from selecting elements that are lower in
	// the Hierarchy than object with the script.

	// In simple words, you don't select one blade of grass when you click on it,
	// but the whole tuft.
}
