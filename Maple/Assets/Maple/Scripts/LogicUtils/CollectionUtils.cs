﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtension
{
	public static List<T> AddS<T>(this List<T> list, T item)
	{
		list.Add(item);
		return list;
	}
	public static List<T> AddIf<T>(this List<T> list, T item, bool cond)
	{
		if (cond) list.Add(item);
		return list;
	}
}

public class CollectionUtils
{
	
}
