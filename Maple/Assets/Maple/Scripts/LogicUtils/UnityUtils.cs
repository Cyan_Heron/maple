﻿using System;
using UnityEngine;

public static class GameObjectExtension
{
	public static void RemoveChildren(this GameObject go)
	{
		foreach (Transform child in go.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
	}
}

public class UnityUtils
{

}


 