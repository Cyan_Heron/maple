﻿using System;
using UnityEngine;

public class MathUtils
{
	public static float INV_SQRT2 = 0.707106781f;

	public static double BetaFunction(double x, double ecoeff, double powercoeff)
	{
		double adjustor = Math.Pow(ecoeff * Math.E, -powercoeff); // 1+e-b
		double conjuctor = Math.Pow(ecoeff * Math.E, (-2.0 * x + 1.0) * powercoeff);// 1+e-2bx+b


		return (adjustor - conjuctor) / ((1.0-adjustor) * (1.0+conjuctor)) + 1.0;
	}

	public static double SignThreshold(double arg, double th)
	{
		if (arg > th) return 1.0;
		if (arg < -th) return -1.0;
		return 0;
	}

	public static Vector3 RotateVector3(Vector3 vector, float angle) // angle is in Degrees
	{
		float costh = Mathf.Cos(Mathf.Deg2Rad * angle);
		float sinth = Mathf.Sin(Mathf.Deg2Rad * angle);

		return new Vector3(
			vector.x * costh - vector.z * sinth,
			vector.y,
			vector.x * sinth + vector.z * costh
			);
	}
}

