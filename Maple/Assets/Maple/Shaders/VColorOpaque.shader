﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
/*
Shader "Custom/VColorOpaque" {
	Category {
	Tags { "Queue" = "Geometry+1" }
	Lighting Off
	
	SubShader {
		
		Pass {
			Tags { "Queue" = "Geometry+1" }
			ZWrite On
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG 
		}
	}	
}
}

*/
Shader "Lines/Colored Blended" {
	SubShader{
		Tags { "RenderType" = "Opaque" }
		ZWrite On
		ZTest LEqual
		Pass {
			Cull Off
			Fog { Mode Off }
			BindChannels {
				Bind "vertex", vertex Bind "color", color
			}
		}
	}
}

/*

Shader "Transparent/Unlit/Without Alphatest" {
	
	Category{
		Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
		Lighting Off
		Cull Off
		ZWrite On // To keep the correct Z order
		Blend SrcAlpha OneMinusSrcAlpha // Doing Alpha Blending
	}
}
*/